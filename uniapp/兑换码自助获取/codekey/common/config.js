// 通讯密匙
const websiteSecret = '';
const websiteUrl = 'https://xxxx/api.php';
const cachePath = '/cache/';
// 裂变活动页面域名 本H5站点域名
const webBaseUrl = 'https://xxxx/';

var getUrlName = function getUrlName(url) {
	let tmp= new Array();//临时变量，保存分割字符串
	tmp = url.split("/");//按照"/"分割
	let pp = tmp[tmp.length-1];//获取最后一部分，即文件名和参数
	tmp = pp.split("?");//把参数和文件名分割开
	return tmp[0];
}

export default {  
    websiteSecret,
    websiteUrl,
    webBaseUrl,
    cachePath,
	getUrlName,
}  