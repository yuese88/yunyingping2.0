<?php

namespace app\upgrade\admin;

use app\system\admin\Admin;

class UpgradeFile extends Admin
{

    protected $oneModel = 'UpgradeFile';

    protected function initialize()
    {
        parent::initialize();
        $this->UpgradeFileModel = model('UpgradeFile');
        $this->UpgradeVersionModel = model('UpgradeVersion');
    }

    public function index()
    {
        if ($this->request->isAjax()) {
            $map = $data = [];
            $page   = input('param.page/d', 1);
            $limit  = input('param.limit/d', 15);
            $status = input('param.status');

            if (is_numeric($status)) {
                $map['status'] = $status;
            }
            $data = $this->UpgradeFileModel->pageList($map, true, 'create_time desc', $page, $limit, ['location_text']);
            return $this->success('获取成功', '', $data);
        }
        return $this->fetch();
    }

    /**
     * 上传版本包 zip
     *
     * @return void
     * @author 617 <email：723875993@qq.com>
     */
    public function uploadFile()
    {
        if (request()->isPost()) {
            $file = request()->file('version_package');
            // 移动到框架应用根目录/uploads/ 目录下
            $base_file = './upload/upgrade/';
            $info = $file->move($base_file, '');
            if ($info) {
                $result = $this->UpgradeFileModel->uploadFile($info, $base_file);
                if (false === $result) {
                    return json(['code' => 1, 'msg' => $this->UpgradeFileModel->getError()]);
                }
                return json(['code' => 0, 'msg' => '上传成功', 'data' => $result]);
            } else {
                return json(['code' => 1, 'msg' => $file->getError()]);
            }
        }
    }

    /**
     * 保存版本信息
     *
     * @return void
     * @author 617 <email：723875993@qq.com>
     */
    public function edit()
    {
        if (request()->isPost()) {
            $_data = input();
            $result = $this->UpgradeVersionModel->uploadVersion($_data);
            if (false === $result) {
                $this->error($this->UpgradeVersionModel->getError());
            }
            $this->success('保存成功');
        }
        return $this->fetch('form');
    }
}
