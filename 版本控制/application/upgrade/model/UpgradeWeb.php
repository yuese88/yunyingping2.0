<?php

namespace app\upgrade\model;

use app\member\model\Member;

class UpgradeWeb extends Base
{

    public function getUsernameAttr($v, $d)
    {
        return (new Member)->getFieldById($d['member_id'], 'username');
    }
}
