<?php
namespace app\member\api;
use app\member\model\Login as LoginModel;
use app\member\model\MemberAccount;

class Member extends MemberInit
{
    public function initialize() {
        parent::initialize();
    }
    //基础信息
    public function get_info()
    {
        return $this->_success('',$this->member);
    }
    //修改会员头像
    public function modify_headimg()
    {
        # code...
    }
    //修改昵称
    public function modify_nickname()
    {
        # code...
    }
    //修改手机号
    public function modify_mobile()
    {
        # code...
    }
    //修改密码
    public function modify_password()
    {
        # code...
    }
    //修改支付密码
    public function modify_pay_password()
    {
        # code...
    }
}