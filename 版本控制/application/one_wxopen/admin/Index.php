<?php
namespace app\one_wxopen\admin;
use app\system\admin\Admin;

class Index extends Admin
{
    public function index()
    {
        $path= env('app_path').implode(DIRECTORY_SEPARATOR,['one_wxopen','lib','WeTool','Cache']).DIRECTORY_SEPARATOR;
        $data = [
            'ticket'=>false,
            'access_token'=>false,
        ];
        $file = $path.'component_verify_ticket';
        if (file_exists($file) && ($content = file_get_contents($file))) {
            $data['ticket'] = unserialize($content);
            $data['ticket']['expired_format'] = date('Y-m-d H:i:s',$data['ticket']['expired']);
        }
        $file = $path.'wechat_component_access_token';
        if (file_exists($file) && ($content = file_get_contents($file))) {
            $data['access_token'] = unserialize($content);
            $data['access_token']['expired'] -= 7000;
            $data['access_token']['expired_format'] = date('Y-m-d H:i:s',($data['access_token']['expired']));
        }
        return $this->assign(compact('data'))->fetch();
    }
}