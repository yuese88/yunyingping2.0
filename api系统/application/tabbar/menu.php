<?php
/**
 * 模块菜单
 * 字段说明
 * url 【链接地址】格式：tabbar/控制器/方法，可填写完整外链[必须以http开头]
 * param 【扩展参数】格式：a=123&b=234555
 */
return [
    [
        'pid'           => 0,
        'is_menu'       => '1',
        'title'         => '底部菜单',
        'icon'          => 'mdi mdi-dropbox',
        'module'        => 'tabbar',
        'url'           => 'tabbar',
        'param'         => '',
        'target'        => '_self',
        'nav'           => 1,
        'sort'          => 100,
        'childs'         => [
            [
                'title' => '菜单列表',
                'icon' => 'fa fa-credit-card',
                'module' => 'tabbar',
                'url' => 'tabbar/index/index',
                'param' => '',
                'target' => '_self',
                'debug' => 0,
                'system' => 0,
                'nav' => 1,
                'sort' => 0,
                'childs' => [
                    [
                    'title' => '编辑',
                    'icon' => 'fa fa-credit-card',
                    'module' => 'tabbar',
                    'url' => 'tabbar/index/edit',
                    'param' => '',
                    'target' => '_self',
                    'debug' => 0,
                    'system' => 0,
                    'nav' => 0,
                    'sort' => 0,
                    ],
                ],
            ],
            [
                'title' => '菜单类型列表',
                'icon' => 'fa fa-credit-card',
                'module' => 'tabbar',
                'url' => 'tabbar/type/index',
                'param' => '',
                'target' => '_self',
                'debug' => 0,
                'system' => 0,
                'nav' => 1,
                'sort' => 0,
                'childs' => [
                    [
                    'title' => '编辑',
                    'icon' => 'fa fa-credit-card',
                    'module' => 'tabbar',
                    'url' => 'tabbar/type/edit',
                    'param' => '',
                    'target' => '_self',
                    'debug' => 0,
                    'system' => 0,
                    'nav' => 0,
                    'sort' => 0,
                    ],
                ],
            ],
        ]
    ],
];