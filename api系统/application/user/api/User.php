<?php

namespace app\user\api;

use app\one_api\api\UserInit;
use app\user\server\User as UserService;

class User extends UserInit
{
    public function initialize()
    {
        if (request()->action() == 'feedback') {
            $this->check_login = false;
        }
        parent::initialize();
        if (!isset($this->apiKey) || empty($this->apiKey) || cache('apiKey') != $this->apiKey) {
            return $this->_error('非法请求', [], 710);
        }
        $this->UserService = new UserService();
    }

    public function changeUsi()
    {
        $data = $this->params;
        $result = $this->UserService->changeUsi($data, $this->user);
        if (false === $result) {
            return $this->_error($this->UserService->getError(), '', 3000);
        }
        return $this->_success('修改成功');
    }

    public function feedback()
    {
        $data = $this->params;
        $result = $this->UserService->feedback($data, $this->user);
        if (false === $result) {
            return $this->_error($this->UserService->getError(), '', 3001);
        }
        return $this->_success('反馈成功');
    }

    public function getFeedbackList()
    {
        $data = $this->params;
        $result = $this->UserService->getFeedbackList($data, $this->user);
        if (false === $result) {
            return $this->_error($this->UserService->getError(), '', 3002);
        }
        return $this->_success('获取成功', $result);
    }
    
    public function getFeedbackDetail()
    {
        $data = $this->params;
        $result = $this->UserService->getFeedbackDetail($data, $this->user);
        if (false === $result) {
            return $this->_error($this->UserService->getError(), '', 3003);
        }
        return $this->_success('获取成功', $result);
    }
    /**
     * 修改手机号
     *
     * @return void
     * @author 617 <email：723875993@qq.com>
     */
    public function changeMobile()
    {
        $data = $this->params;
        $result = $this->UserService->changeMobile($data, $this->user);
        if (false === $result) {
            return $this->_error($this->UserService->getError(), '', 3004);
        }
        return $this->_success('修改成功');
    }
    /**
     * 修改密码
     *
     * @return void
     * @author 617 <email：723875993@qq.com>
     */
    public function changePassword()
    {
        $data = $this->params;
        $result = $this->UserService->changePassword($data, $this->user);
        if (false === $result) {
            return $this->_error($this->UserService->getError(), '', 3005);
        }
        return $this->_success('修改成功');
    }
    /**
     * 用户中心首页
     *
     * @return void
     * @author 617 <email：723875993@qq.com>
     */
    public function home()
    {
        $data = $this->params;
        $result = $this->UserService->home($data, $this->user);
        if (false === $result) {
            return $this->_error($this->UserService->getError(), '', 3006);
        }
        return $this->_success('获取成功', $result);
    }

    public function getNumbers()
    {
        $data = $this->params;
        $result = $this->UserService->getNumbers($data, $this->user);
        if (false === $result) {
            return $this->_error($this->UserService->getError(), '', 3007);
        }
        return $this->_success('获取成功', $result);
    }

    public function bindUser()
    {
        $data = $this->params;
        $result = $this->UserService->bindUser($data, $this->user);
        if (false === $result) {
            return $this->_error($this->UserService->getError(), '', 3008);
        }
        return $this->_success('绑定成功', $result);
    }
    
    public function changeAvatar()
    {
        $data = $this->params;
        $result = $this->UserService->changeAvatar($data, $this->user);
        if (false === $result) {
            return $this->_error($this->UserService->getError(), '', 3008);
        }
        return $this->_success('修改成功', $result);
    }
    
    
}
