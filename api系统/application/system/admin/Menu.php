<?php

namespace app\system\admin;

use app\system\admin\Admin;
use think\facade\Env;

/**
 * 菜单控制器
 * @package app\system\admin
 */
class Menu extends Admin
{
    protected $oneTable = 'SystemMenu';

    public function initialize()
    {
        parent::initialize();
        if (!isset($this->apiKey) || empty($this->apiKey) || cache('apiKey') != $this->apiKey) {
            exit(json_encode(['msg'=>'非法操作！','code'=>710]));
        }
    }

    public function sort()
    {
        $sort = input('sort');
        // 排序记录到system模块下
        $path = Env::get('app_path') . DS . 'system' . DS . 'menudata' . DS;
        if (!is_readable($path)) {
            is_file($path) or mkdir($path, 0700);
        }
        $path = $path . ADMIN_ID . '.sort';
        $file = new \one\WriteIniFile($path);
        if (is_file($path)) {
            $file->update($sort)->write();
            return $this->success('排序修改成功', '');
        }
        $file->create($sort)->write();
        return $this->success('排序设置成功', '');
    }
}
