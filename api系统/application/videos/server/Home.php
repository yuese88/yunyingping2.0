<?php
namespace app\videos\server;

use app\common\server\Service;
use app\banner\model\Ad as AdModel;
use app\news\model\Msg as MsgModel;

class Home extends Service{

    public function initialize() {
        parent::initialize();
        if (!isset($this->serviceKey) || empty($this->serviceKey) || cache('serviceKey') != $this->serviceKey) {
            exit(json_encode(['msg'=>'非法操作！','code'=>712]));
        }
        $this->appid = config('wechat.appid');
        $this->secret = config('wechat.secret');
        $this->AdModel = new AdModel();
        $this->MsgModel = new MsgModel();
    }

    public function getGuideImg() {
        $map = [];
        $map[] = ['status', 'eq', 1];
        $map[] = ['type', 'eq', 1];
        $list = $this->AdModel->getList($map, 0);
        return $list;
    }
    /**
     * 随机取首页广告
     *
     * @return void
     * @author 617 <email：723875993@qq.com>
     */
    public function getHomeAdList($data, $user) {
        $map = [];
        $map[] = ['status', 'eq', 1];
        $map[] = ['type', 'eq', 2];
        $list = $this->AdModel->getList($map, 0);
        return $list;
    }
    /**
     * 随机取播放页广告
     *
     * @return void
     * @author 617 <email：723875993@qq.com>
     */
    public function getPlayerAdList($data, $user) {
        $_data['alert_ad'] = $this->AdModel->getOne(3, 1, $user);
        $_data['alert_public'] = $this->AdModel->getOne(4, 1, $user);
        $_data['video_ad'] = $this->AdModel->getOne(5, 1, $user);
        $_data['play_start_ad'] = $this->AdModel->getOne(6, 1, $user);
        return $_data;
    }
    /**
     * 随机取一条常驻广告
     *
     * @return void
     * @author 617 <email：723875993@qq.com>
     */
    public function getPublicAdList($data, $user) {
        return $this->AdModel->getOne(4, 1, $user);
    }
    /**
     * 随机取一条激励广告
     *
     * @return void
     * @author 617 <email：723875993@qq.com>
     */
    public function getAdList($data, $user) {
        // 弹窗广告
        $_data['alert_ad'] = $this->AdModel->getOne(3, 5, $user);
        // 播放页上部
        $_data['video_top'] = $this->AdModel->getOne(8, 5, $user);
        // 播放页下部
        $_data['alert_public'] = $this->AdModel->getOne(4, 5, $user);
        // 视频广告
        $_data['video_ad'] = $this->AdModel->getOne(5, 5, $user);
        // 播放前广告
        $_data['play_start_ad'] = $this->AdModel->getOne(6, 5, $user);
        // 壁纸广告
        $_data['wallpaper_ad'] = $this->AdModel->getOne(7, 5, $user);
        // 全局普通广告（banner、原生格子广告等）
        $_data['common_ad'] = $this->AdModel->getOne(2, 10, $user);
        return $_data;
    }

    public function about()
    {
        return config('ucenter.about_app');
    }

    public function punch()
    {
        return config('ucenter.punch');
    }

    public function getMsgLists()
    {
        $map = [];
        $map[] = ['status', 'eq', 1];
        $list = $this->MsgModel->getList($map, 0);
        return $list;
    }

    /**
     * 图片等级检测
     *
     * @return void
     * @author 617 <email：723875993@qq.com>
     */
    public function checkImg($data)
    {
        $accessToken = cache('app_access_token');
        if (!$accessToken) {
            $url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid={$this->appid}&secret={$this->secret}";
            $result = wx_send($url);
            $result = json_decode($result, 1);
            if (isset($result['errcode'])) {
                $this->error = $result['errmsg'];
                return false;
            }
            $accessToken = $result['access_token'];
            cache('app_access_token', $accessToken, 7000);
        }
        if ($accessToken) {
            $url = "https://api.weixin.qq.com/wxa/img_sec_check?access_token=$accessToken";
            $img_data = base64_image_content($data['str'], '', true);
            if ($img_data) {
                $postData = [
                    'media' => curl_file_create($img_data)
                ];
                $result = wx_send($url, $postData, 'POST', [], 1);
                $result = json_decode($result, 1);
                if ($result['errcode'] == 0) {
                    return true;
                }
                $this->error = '图片含有违规信息';
                return false;
            }
            $this->error = '图片大小不能超过1M';
            return false;
        }
        $this->error = '调用接口失败';
        return false;
    }
}